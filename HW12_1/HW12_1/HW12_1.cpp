﻿
#include "pch.h"
#include <iostream>

// Функция печати (std::cout)
void print()
{
	std::cout << "Hello SkillBox!\n";
}

/*
*   Присвоение и печать значений
*/
int main()
{
	print();

	int x = 100;
	std::cout << "x = " << x << "\n";

	int y = x + 100;
	std::cout << "y = " << y << "\n";

	int mult = x * y;
	std::cout << "mult = " << mult << "\n";

	int b = 0;
	b = b + 2;
	std::cout << "b = " << b << "\n";
}